package no.uib.ii.inf112;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;

public class TestTest {
	TextAligner aligner = new TextAligner() {

		public String center(String text, int width) {
			int extra = (width - text.length()) / 2;
			return " ".repeat(extra) + text + " ".repeat(extra);

		}

		public String flushRight(String text, int width) {
			int extra = width - text.length();


			return " ".repeat(extra) + text;
		}

		public String flushLeft(String text, int width) {
			int extra = width - text.length();


			return text + " ".repeat(extra);
		}

		public String justify(String text, int width) {

			ArrayList<Character> chars = new ArrayList<Character>();
			for (char c : text.toCharArray()) {
				chars.add(c);
			}

			int i = 0;
			while (chars.size() < width) {
				if (chars.get(i) == ' ') {
					chars.add(i, ' ');
					i = findNextNonSpace(i, chars);
				}
				i = (i + 1) % chars.size();
			}

			String ans = "";
			for (char c : chars) {
				ans += c;
			}

			return ans;

		}

		private int findNextNonSpace(int i, ArrayList<Character> chars) {
			int j = i;
			while (true) {
				if (chars.get(j) != ' ') {
					return j;
				}
				j += 1;
			}
		}
	};

	@Test
	void testCenter() {
		assertEquals("  A  ", aligner.center("A", 5));
		assertEquals(" foo ", aligner.center("foo", 5));

	}

	@Test
	void testFlushRight() {
		assertEquals("    A", aligner.flushRight("A", 5));
		assertEquals("     ABB", aligner.flushRight("ABB", 8));
		assertEquals("  ABABABAB", aligner.flushRight("ABABABAB", 10));


	}

	@Test
	void testFlushLeft() {
		assertEquals("A    ", aligner.flushLeft("A", 5));
		assertEquals("ABB     ", aligner.flushLeft("ABB", 8));
		assertEquals("ABABABAB  ", aligner.flushLeft("ABABABAB", 10));
	}

	@Test
	void testJustify() {
		assertEquals("fee   fie   foo", aligner.justify("fee fie foo", 15));
	}



}
